package fi.vamk.e1901111.semodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemodemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemodemoApplication.class, args);
	}

}
