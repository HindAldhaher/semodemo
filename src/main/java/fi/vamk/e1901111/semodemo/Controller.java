package fi.vamk.e1901111.semodemo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
class Controller {
    @RequestMapping("/test")
    public String test() {
        return "{\"id\":1}";
    }
}
